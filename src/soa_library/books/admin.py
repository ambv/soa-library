from django.contrib import admin

from soa_library.books.models import Book, Author

class BookAdmin(admin.ModelAdmin):
    list_display = ['name']

admin.site.register(Book, BookAdmin)

class AuthorAdmin(admin.ModelAdmin):
    list_display = ['last_name', 'first_name']
    list_filter = ['last_name']
    search_fields = ['last_name', 'first_name']

admin.site.register(Author, AuthorAdmin)
