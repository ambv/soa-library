from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.resources import ModelResource

from soa_library.books.models import Author, Book

class AuthorResource(ModelResource):
    class Meta:
        queryset = Author.objects.all()
        authentication = ApiKeyAuthentication()
        filtering = {
            'id': ALL,
            'first_name': ALL,
            'last_name': ALL,
        }

class BookResource(ModelResource):
    authors = fields.ToManyField(AuthorResource, 'authors', null=True, full=True)

    class Meta:
        queryset = Book.objects.all()
        authentication = ApiKeyAuthentication()
        filtering = {
            'id': ALL,
            'name': ALL,
            'authors': ALL_WITH_RELATIONS,
        }
