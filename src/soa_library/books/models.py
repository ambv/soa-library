from django.db import models as db


class Author(db.Model):
    first_name = db.CharField(max_length=100)
    last_name = db.CharField(max_length=100)

    class Meta:
        ordering = ['last_name', 'first_name']

    def __unicode__(self):
        return "{} {}".format(self.first_name, self.last_name)

class Book(db.Model):
    name = db.CharField(max_length=100)
    authors = db.ManyToManyField(Author)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name
