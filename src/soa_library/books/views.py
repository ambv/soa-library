# Create your views here.

from django.http import HttpResponse

def dummy_view(request):
    return HttpResponse("Hello.",
                        mimetype="text/plain")
