from datetime import datetime
import json

from celery.task import task
from django.conf import settings
from django.db import models as db
import slumber

BOOK_REPOSITORY = settings.BOOK_REPOSITORY

class Customer(db.Model):
    first_name = db.CharField(max_length=100)
    last_name = db.CharField(max_length=100)

    class Meta:
        ordering = ['last_name', 'first_name']

    def __unicode__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Rental(db.Model):
    book = db.URLField(unique=True)
    book_cache = db.TextField(null=True, blank=True)
    customer = db.ForeignKey(Customer)
    rental_dt = db.DateTimeField(default=datetime.now)
    return_dt = db.DateTimeField(default=None, blank=True, null=True)

    class Meta:
        ordering = ['rental_dt', 'customer']

    def __unicode__(self):
        return '{}: {}'.format(self.customer, self.book)

    @staticmethod
    @task()
    def rent(customer_id, book_id):
        url = BOOK_REPOSITORY['url']
        creds = dict(BOOK_REPOSITORY)
        del creds['url']
        api = slumber.API(url)
        book = api.book(book_id).get(**creds)
        customer = Customer.objects.using('rental').get(pk=customer_id)
        new_rental = Rental(book=book['resource_uri'], book_cache=json.dumps(book),
            customer=customer)
        new_rental.save(using='rental')
        return new_rental.id
